declare module 'activity-detector' {
  // https://github.com/tuenti/activity-detector#advanced-options

  type Events =
    | 'click'
    | 'mousemove'
    | 'keydown'
    | 'DOMMouseScroll'
    | 'mousewheel'
    | 'mousedown'
    | 'touchstart'
    | 'touchmove'
    | 'focus';
  interface Opts {
    timeToIdle: number; // milliseconds of inactivity which makes activity detector transition to 'idle' (30000 by default)
    initialState?: 'idle' | 'active'; // "active" by default,
    ignoredEventsWhenIdle?: Events[]; // events to ignore in idle state. By default: ['mousemove']
  }

  interface Detector {
    on(event: string, cb: () => void): void;
  }

  export default function createActivityDetector(opts: Opts): Detector;
}
