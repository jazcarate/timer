import React from 'react';
import { AccionIndividual, timer } from './domain/timers.reducer';

const noop: React.Dispatch<AccionIndividual> = () => {};

const TimersContext = React.createContext({ timer: timer(), despacha: noop });

export default TimersContext;
