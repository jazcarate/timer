# Timer

[![pipeline status](https://gitlab.com/jazcarate/timer/badges/master/pipeline.svg)](https://gitlab.com/jazcarate/timer/commits/master)
[![coverage report](https://gitlab.com/jazcarate/timer/badges/master/coverage.svg)](https://gitlab.com/jazcarate/timer/commits/master)

## Contribute

If you are thinking

> This app is missing \_\_\_

Thank you for your input! I'd very much would like to hear it at this [repo's ticket tracker](https://gitlab.com/jazcarate/timer/issues/new), or via email at [j@florius.com.ar](mailto:j@florius.com.ar?subject=Timers--Feedback!).

If you are _so awesome_ so as to code your feedback, feel free to make a merge request on this repo. I'll be delighted to have you as a contributor!

## Objective

In a Magic: the Gathering tournament we usually need to show a big round timer. Each round is 50 minutes long, and we occasionally need to show multiple clocks (for other events in the same venue). For this, we need an app that has:

- Offline support (WiFi is scarce on these venues).
- Multiple timers
- A minimalist and intuitive UI (i.e.: keep the text to a minimum, so events can be run internationally).
- An in between state before counting down, and after re-starting (when players are sitting).
- Counts "up" (after 0, it goes to the negative numbers).

## Usage

This app is hosted at [timers.florius.com.ar](https://timers.florius.com.ar/) via GitLab's pages. So if you just want to use the timer, go to [timers.florius.com.ar](https://timers.florius.com.ar/) free of charge 😺.

## Development

This project is bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Installation

1. Install [`node`](https://nodejs.org/). Check the `.nvmrc` file for the correct version.
2. Run `npm install` to install the dependencies.

### Running the code

Just run

```bash
npm run start
```
